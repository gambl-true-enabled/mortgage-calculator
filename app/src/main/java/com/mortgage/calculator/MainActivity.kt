package com.mortgage.calculator

import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import androidx.ui.tooling.preview.Preview
import com.mortgage.calculator.ui.CalculateContent
import com.mortgage.calculator.ui.HelpContent
import com.mortgage.calculator.ui.HistoryContent
import com.mortgage.calculator.ui.MortgageBottomBar
import com.mortgage.calculator.utils.Screen
import com.mortgage.calculator.utils.TextFieldState
import com.mortgage.calculator.utils.ui.MortgageCalculatorTheme

class MainActivity : AppCompatActivity() {

    private val viewModel: MortgageViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
        setContent {
            MortgageCalculatorTheme {
                Scaffold(
                        modifier = Modifier
                                .fillMaxSize(),
                        bottomBar = {
                            MortgageBottomBar(
                                content = viewModel.bottomBar
                            )
                        }
                ) {
                    val modifierScreen = Modifier
                        .padding(bottom = it.bottom, start = 16.dp, end = 16.dp)
                    viewModel.currentScreen.observeAsState().value?.let { screen ->
                        when(screen) {
                            Screen.CALCULATE -> CalculateContent(
                                modifier = modifierScreen,
                                yearPercentage = remember { viewModel.yearPercentage },
                                mortgageMoth = remember { viewModel.mortgageMoth },
                                mortgagePrice = remember { viewModel.mortgagePrice }
                            )
                            Screen.HISTORY -> HistoryContent(
                                modifier = modifierScreen
                            )
                            Screen.HELP -> HelpContent(
                                modifier = modifierScreen
                            )
                        }
                    }
                }
            }
        }
    }
}
