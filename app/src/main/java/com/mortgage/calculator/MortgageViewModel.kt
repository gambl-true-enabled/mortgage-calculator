package com.mortgage.calculator

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mortgage.calculator.ui.BarIconState
import com.mortgage.calculator.ui.BottomBarModel
import com.mortgage.calculator.utils.Screen
import com.mortgage.calculator.utils.TextFieldState

class MortgageViewModel: ViewModel() {

    val bottomBar: BottomBarModel

    private lateinit var lastSelectedBarIcon: MutableState<BarIconState>

    private val _currentScreen = MutableLiveData<Screen>()
    val currentScreen: LiveData<Screen> = _currentScreen

    val yearPercentage: TextFieldState = TextFieldState()
    val mortgageMoth: TextFieldState = TextFieldState()
    val mortgagePrice: TextFieldState = TextFieldState()

    init {
        val icons = listOf(
            getCalculateIcon(),
//            getHistoryIcon(),
            getHelpIcon()
        )
        bottomBar = BottomBarModel(icons = icons)
    }

    private fun getCalculateIcon(): BottomBarModel.BarIconModel = BottomBarModel.BarIconModel(
        icon = mutableStateOf(R.drawable.ic_calculate),
        state = mutableStateOf(BarIconState.SELECTED)
    ).apply {
        lastSelectedBarIcon = state
        _currentScreen.value = Screen.CALCULATE
        clickCallback = {
            _currentScreen.value = Screen.CALCULATE
            state.select()
        }
    }

    private fun getHistoryIcon(): BottomBarModel.BarIconModel = BottomBarModel.BarIconModel(
        icon = mutableStateOf(R.drawable.ic_history)
    ).apply {
        clickCallback = {
            _currentScreen.value = Screen.HISTORY
            state.select()
        }
    }

    private fun getHelpIcon(): BottomBarModel.BarIconModel = BottomBarModel.BarIconModel(
        icon = mutableStateOf(R.drawable.ic_help)
    ).apply {
        clickCallback = {
            _currentScreen.value = Screen.HELP
            state.select()
        }
    }

    private fun MutableState<BarIconState>.select() {
        if (lastSelectedBarIcon == this)
            return
        lastSelectedBarIcon.value = BarIconState.NON_SELECTED
        this.value = BarIconState.SELECTED
        lastSelectedBarIcon = this
    }
}