package com.mortgage.calculator.ui

import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.mortgage.calculator.R
import com.mortgage.calculator.utils.TextFieldState
import kotlin.math.pow

@Composable
fun CalculateContent(
    yearPercentage: TextFieldState = remember { TextFieldState() },
    mortgageMoth: TextFieldState = remember { TextFieldState() },
    mortgagePrice: TextFieldState = remember { TextFieldState() },
    modifier: Modifier = Modifier
) {
    val itemModifier = Modifier
        .gravity(Alignment.Start)

    val monthPercentage = if (yearPercentage.text.isEmpty())
        "-"
    else {
        try {
            val yearPercentageInt = yearPercentage.text.toDouble()
            String.format("%.3f", yearPercentageInt / 12 / 100).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    val commonPercentage = if (mortgageMoth.text.isEmpty())
        "-"
    else {
        try {
            val res = (1 + monthPercentage.toDouble()).pow(mortgageMoth.text.toInt())
            String.format("%.2f", res).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    val monthlyPay = if (mortgagePrice.text.isEmpty())
        "-"
    else {
        try {
            val sumPay = mortgagePrice.text.toDouble()
            val monthPay = monthPercentage.toDouble()
            val comPay = commonPercentage.toDouble()
            val res = sumPay * monthPay * comPay / (comPay - 1)
            String.format("%.3f", res).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    val percentagePart = if (mortgagePrice.text.isEmpty())
        "-"
    else {
        try {
            val sumPay = mortgagePrice.text.toDouble()
            val monthPay = monthPercentage.toDouble()
            val res = sumPay * monthPay
            String.format("%.2f", res).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    val mainPart = if (mortgagePrice.text.isEmpty())
        "-"
    else {
        try {
            val sumPay = mortgagePrice.text.toDouble()
            val monthPay = monthPercentage.toDouble()
            val res = monthlyPay.toDouble() - (sumPay * monthPay)
            String.format("%.2f", res).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    val overPay = if (mortgagePrice.text.isEmpty())
        "-"
    else {
        try {
            val res = monthlyPay.toDouble() * mortgageMoth.text.toDouble() - mortgagePrice.text.toDouble()
            String.format("%.2f", res).replace(',', '.')
        } catch (e: Exception) {
            e.printStackTrace()
            "-"
        }
    }

    ScrollableColumn(
        modifier = modifier
    ) {
        NumberTextField(
            modifier = itemModifier,
            hint = R.string.year_percentage,
            state = yearPercentage
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.month_percentage,
            result = monthPercentage
        )
        NumberTextField(
            modifier = itemModifier,
            hint = R.string.mortgage_moth,
            state = mortgageMoth
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.common_percentage,
            result = commonPercentage
        )
        NumberTextField(
            modifier = itemModifier,
            hint = R.string.mortgage_price,
            state = mortgagePrice
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.month_pay,
            result = monthlyPay
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.percentage_part,
            result = percentagePart
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.main_part,
            result = mainPart
        )
        ResultText(
            modifier = itemModifier,
            name = R.string.over_pay,
            result = overPay
        )
    }
}

@Composable
private fun NumberTextField(
    modifier: Modifier = Modifier,
    hint: Int = 0,
    state: TextFieldState = remember { TextFieldState() }
) {
    OutlinedTextField(
        modifier = modifier
            .fillMaxWidth()
            .padding(bottom = 16.dp),
        value = state.text,
        keyboardType = KeyboardType.Number,
        onValueChange = {
            state.text = it
        },
        label = {
            Text(
                text = stringResource(id = hint)
            )
        }
    )
}

@Composable
private fun ResultText(
    modifier: Modifier = Modifier,
    name: Int = 0,
    result: String = ""
) {
    Row(
        modifier = modifier
            .padding(bottom = 8.dp)
    ) {
        Text(text = stringResource(id = name) + ": ")
        Text(text = result)
    }
}
