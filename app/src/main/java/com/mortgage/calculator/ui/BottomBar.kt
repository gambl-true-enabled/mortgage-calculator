package com.mortgage.calculator.ui

import androidx.compose.animation.ColorPropKey
import androidx.compose.animation.core.FloatPropKey
import androidx.compose.animation.core.transitionDefinition
import androidx.compose.animation.core.tween
import androidx.compose.animation.transition
import androidx.compose.foundation.Box
import androidx.compose.foundation.Icon
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.RowScope.gravity
import androidx.compose.foundation.layout.RowScope.weight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.BottomAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp

data class BottomBarModel(
    val icons: List<BarIconModel> = emptyList()
) {
    data class BarIconModel(
        val icon: MutableState<Int> = mutableStateOf(-1),
        val state: MutableState<BarIconState> = mutableStateOf(BarIconState.NON_SELECTED),
        var clickCallback: () -> Unit = {}
    )
}

@Composable
fun MortgageBottomBar(
    modifier: Modifier = Modifier,
    content: BottomBarModel = BottomBarModel()
) {
    val itemsDefModifier = Modifier
        .weight(1f)
    BottomAppBar(
        modifier = modifier
            .clip(RoundedCornerShape(topLeft = 16.dp, topRight = 16.dp))
    ) {
        content.icons.forEach { icon ->
            BarIcon(
                modifier = itemsDefModifier,
                icon = icon
            )
        }
    }
}

enum class BarIconState{
    SELECTED,
    NON_SELECTED
}

private val colorKey = ColorPropKey()

private val selectionTransitionDefinition = transitionDefinition<BarIconState> {
    state(BarIconState.SELECTED) {
        this[colorKey] = Color.White
    }
    state(BarIconState.NON_SELECTED) {
        this[colorKey] = Color.Black
    }
    val duration = 250
    transition(fromState = BarIconState.SELECTED) {
        colorKey using tween(
            durationMillis = duration
        )
    }
    transition(fromState = BarIconState.NON_SELECTED) {
        colorKey using tween(
            durationMillis = duration
        )
    }
}

@Composable
private fun BarIcon(
    modifier: Modifier = Modifier,
    icon: BottomBarModel.BarIconModel = BottomBarModel.BarIconModel()
) {
    val transition = transition(
        definition = selectionTransitionDefinition,
        toState = icon.state.value
    )
    Box(
        modifier = modifier
            .clip(RoundedCornerShape(12.dp))
            .clickable(onClick = icon.clickCallback),
    ) {
        Icon(
            asset = vectorResource(id = icon.icon.value),
            modifier = Modifier
                .gravity(Alignment.CenterHorizontally),
            tint = transition[colorKey]
        )
    }
}