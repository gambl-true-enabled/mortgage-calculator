package com.mortgage.calculator.ui

import androidx.annotation.StringRes
import androidx.compose.foundation.ProvideTextStyle
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.mortgage.calculator.R
import com.mortgage.calculator.utils.ui.typography

@Composable
fun HelpContent(
    modifier: Modifier = Modifier
) {
    val itemsModifier = Modifier
    ScrollableColumn(
        modifier = modifier
    ) {
        Header(text = R.string.help_header1, modifier = itemsModifier)
        Body(text = R.string.help_body1, modifier = itemsModifier)

        Header(text = R.string.help_header2, modifier = itemsModifier)
        SubHeader(text = R.string.help_subheader2, modifier = itemsModifier)
        Body(text = R.string.help_body2, modifier = itemsModifier)

        SubHeader(text = R.string.help_subheader3, modifier = itemsModifier)
        Body(text = R.string.help_body3, modifier = itemsModifier)

        SubHeader(text = R.string.help_subheader4, modifier = itemsModifier)
        Body(text = R.string.help_body4, modifier = itemsModifier)

        SubHeader(text = R.string.help_subheader5, modifier = itemsModifier)
        Body(text = R.string.help_body5, modifier = itemsModifier)
    }
}

@Composable
private fun Header(
    @StringRes text: Int,
    modifier: Modifier = Modifier
) {
    ProvideTextStyle(value = typography.h1) {
        Text(
            text = stringResource(id = text),
            modifier = modifier
                .padding(bottom = 16.dp)
        )
    }
}

@Composable
private fun SubHeader(
    @StringRes text: Int,
    modifier: Modifier = Modifier
) {
    ProvideTextStyle(value = typography.h2) {
        Text(
            text = stringResource(id = text),
            modifier = modifier
                .padding(bottom = 8.dp)
        )
    }
}

@Composable
private fun Body(
    @StringRes text: Int,
    modifier: Modifier = Modifier
) {
    ProvideTextStyle(value = typography.body1) {
        Text(
            text = stringResource(id = text),
            modifier = modifier
                .padding(bottom = 8.dp)
        )
    }
}