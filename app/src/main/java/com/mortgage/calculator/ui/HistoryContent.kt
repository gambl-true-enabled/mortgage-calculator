package com.mortgage.calculator.ui

import androidx.compose.foundation.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun HistoryContent(
    modifier: Modifier = Modifier
) {
    Text(text = "History")
}